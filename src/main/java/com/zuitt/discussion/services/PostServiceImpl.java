package com.zuitt.discussion.services;

import com.zuitt.discussion.Repositories.PostRepositories;
import com.zuitt.discussion.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service

public class PostServiceImpl implements PostService{
//  An object be instantiated from interfaces;
//  Autowired allow us to use the interface as if it was an instance of an object and allows us to use the methods from the CrudRepositories;

    @Autowired
    private PostRepositories postRepository;

//  Create post
    public void createPost(Post post){
        postRepository.save(post);
    }
//  Get all post

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
//  Delete post
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post successfully deleted", HttpStatus.OK);
    }
    public ResponseEntity updatePost(Long id, Post post){
        Post update = postRepository.findById(id).get();
        //Updating content of the database
        update.setTitle(post.getTitle());
        update.setContent(post.getContent());
        postRepository.save(update);
        return new ResponseEntity<>("Post updated successfully",HttpStatus.OK);
    }




}
