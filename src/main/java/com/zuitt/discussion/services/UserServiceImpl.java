package com.zuitt.discussion.services;

import com.zuitt.discussion.Repositories.UserRepository;
import com.zuitt.discussion.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository repo;
    //Create User
    public void createUser(User user){
        repo.save(user);
    }
    public Iterable<User> getAllUser(){
       return repo.findAll();
    }
    public ResponseEntity deleteUser(Long id){
        repo.deleteById(id);
        return new ResponseEntity<>("User successfully deleted", HttpStatus.OK);
    }
    public ResponseEntity updateUser(Long id,User user){
        User update = repo.findById(id).get();
        //Updating content of the database
        update.setUsername(user.getUsername());
        update.setPassword(user.getPassword());
        repo.save(update);
        return new ResponseEntity<>("User updated successfully",HttpStatus.OK);
    }
}
